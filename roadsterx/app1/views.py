from django.shortcuts import render

# Create your views here.


from rest_framework import viewsets
from  .serializers import carsSerializer,UserSerializer
from .models import cars
from django.contrib.auth.models import User
from .permissions import *

class carsViewset(viewsets.ModelViewSet):
    queryset = cars.objects.all()
    serializer_class = carsSerializer
    permission_classes = (Isstaff,)


class Userviewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer





