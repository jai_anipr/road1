from app1.models import cars
from rest_framework import serializers
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from .models import groups1

class carsSerializer(serializers.ModelSerializer):
    class Meta:
        model = cars
        fields =  '__all__'



class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = groups1
        fields = ('name',)

class UserSerializer(serializers.ModelSerializer):


    class Meta:
        model = User
        fields = '__all__' #('id', 'username','last_name','first_name','groups')
