from django.db import models
from django.contrib.auth.models import Group
# Create your models here.

class cars(models.Model):
    car_make = models.CharField(max_length=200)
    car_model = models.CharField(max_length=200)
    car_price = models.IntegerField()
    car_registration_year = models.DateField(auto_now=False, auto_now_add=False,blank=True,null=True)

    PETROL = 'PE'
    DIESEL = 'SD'
    LPG = 'LP'
    ELECTRIC ='EL'

    CAR_FUEL_CHOICES = (
        (PETROL, 'Petrol'),
        (DIESEL, 'Diesel'),
        (LPG, 'LPG'),
        (ELECTRIC, 'Electric'),
    )
    car_fuel = models.CharField(max_length=2,choices=CAR_FUEL_CHOICES,default=PETROL)

    HATCHBACK = 'HB'
    SEDAN = 'SD'
    COMPACTSUV = 'CS'
    SUV = 'SU'
    CAR_TYPE_CHOICES = (
        (HATCHBACK, 'Hatchback'),
        (SEDAN, 'Sedan'),
        (COMPACTSUV, 'compact suv'),
        (SUV, 'SUV'),
    )

    car_type = models.CharField(max_length=2,
                     choices=CAR_TYPE_CHOICES,
                     default=HATCHBACK)

    car_in_showroom = models.DateField(auto_now_add=False,auto_now=False,blank=True,null=True)

    def __str__(self):
        return self.car_model



class groups1(Group):

    def __str__(self):
        return self.name



