from rest_framework import permissions
from rest_framework.permissions import BasePermission, SAFE_METHODS
from django.contrib.auth.models import Group


# customers_group = Group.objects.get(name = 'customers')
class Iscustomer(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user and request.user.groups.filter(name='customers'):
            return True
        return False


class Isstaff(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user:
            if request.user.groups.filter(name='staff members'):
                return True
            return False

    """
    def has_object_permission(self, request, view, obj):
        if not request.method in permissions.SAFE_METHODS:
            if request.user and request.user.groups.filter(name='customers'):
                return True
            return False
        return True

    """
"""
class Groupbasepermission(BasePermission):

    group_name = ""

    def check_permissions(self,user):
        "should simply written or raise error"
        try:
            if user.groups.get(name = self.group_name)
        except Group.DoesNotExist:
            raise Http404
"""