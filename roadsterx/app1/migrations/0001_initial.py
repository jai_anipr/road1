# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='cars',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('car_make', models.CharField(max_length=200)),
                ('car_model', models.CharField(max_length=200)),
                ('car_price', models.IntegerField()),
                ('car_registration_year', models.DateField(null=True, blank=True)),
                ('car_fuel', models.CharField(default=b'PE', max_length=2, choices=[(b'PE', b'Petrol'), (b'SD', b'Diesel'), (b'LP', b'LPG'), (b'EL', b'Electric')])),
                ('car_type', models.CharField(default=b'HB', max_length=2, choices=[(b'HB', b'Hatchback'), (b'SD', b'Sedan'), (b'CS', b'compact suv'), (b'SU', b'SUV')])),
                ('car_in_showroom', models.DateField(null=True, blank=True)),
            ],
        ),
    ]
